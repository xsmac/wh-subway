package com.unistrong;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import org.junit.jupiter.api.Test;

public class TestAutoGenerate {
    @Test
    public void autoGenerate() {
        // Step1：代码生成器
        AutoGenerator mpg = new AutoGenerator();

        // Step2：全局配置
        GlobalConfig gc = new GlobalConfig();
        // 填写代码生成的目录(需要修改)
        String projectPath = "E:\\myProject2";
        // 拼接出代码最终输出的目录
        gc.setOutputDir(projectPath + "/src/main/java");
        // 配置开发者信息（可选）（需要修改）
        gc.setAuthor("menu");
        // 配置是否打开目录，false 为不打开（可选）
        gc.setOpen(false);
        // 实体属性 Swagger2 注解，添加 Swagger 依赖，开启 Swagger2 模式（可选）
        gc.setSwagger2(true);
        // 重新生成文件时是否覆盖，false 表示不覆盖（可选）
        gc.setFileOverride(false);
        // 配置主键生成策略，此处为 ASSIGN_ID（可选）
        gc.setIdType(IdType.ASSIGN_ID);
        // 配置日期类型，此处为 ONLY_DATE（可选）
        gc.setDateType(DateType.ONLY_DATE);
        // 默认生成的 service 会有 I 前缀
        gc.setServiceName("%sService");
        mpg.setGlobalConfig(gc);

        // Step3：数据源配置（需要修改）
        DataSourceConfig dsc = new DataSourceConfig();
        // 配置数据库 url 地址
        dsc.setUrl("jdbc:oracle:thin:@192.168.19.85:1521:orcl");
        // dsc.setSchemaName("testMyBatisPlus"); // 可以直接在 url 中指定数据库名
        // 配置数据库驱动
        dsc.setDriverName("oracle.jdbc.driver.OracleDriver");
        // 配置数据库连接用户名
        dsc.setUsername("wh_train");
        // 配置数据库连接密码
        dsc.setPassword("wh_train");
        mpg.setDataSource(dsc);

        // Step:4：包配置
        PackageConfig pc = new PackageConfig();
        // 配置父包名（需要修改）
        pc.setParent("com.hitown");
        // 配置模块名（需要修改）
        pc.setModuleName("");
        // 配置 entity 包名
        pc.setEntity("model.train");
        // 配置 mapper 包名
        pc.setMapper("dao.train");
        // 配置 service 包名
        pc.setService("service.train");
        // 配置 controller 包名
        pc.setController("controller.train");
        mpg.setPackageInfo(pc);

        // Step5：策略配置（数据库表配置）
        StrategyConfig strategy = new StrategyConfig();
        // 指定表名（可以同时操作多个表，使用 , 隔开）（需要修改）
        // strategy.setInclude("DXHD_ABLX,DXHD_ABLXGJ,DXHD_ABRW,DXHD_ABRW_ZXJJ,DXHD_ABRY,DXHD_ABRY_MJ,DXHD_ACTIVITY,DXHD_BAGS");
        //strategy.setInclude("DXHD_ABLX,DXHD_ABLXGJ,DXHD_ABRW,DXHD_ABRW_ZXJJ,DXHD_ABRY,DXHD_ABRY_MJ,DXHD_ACTIVITY,DXHD_BAGS,DXHD_CGDA,DXHD_CHRY,DXHD_CLTS_DQC,DXHD_CLTS_JC,DXHD_CLTS_JTLK,DXHD_CLTS_SHCL,DXHD_CLTS_TPCL,DXHD_CLTS_WBCL,DXHD_CLTS_ZDCLTJ,DXHD_CLTS_ZDCLWZ,DXHD_CLTS_ZDRCL,DXHD_DMX,DXHD_FSP_FSP,DXHD_FSP_ZZXX,DXHD_HDZG,DXHD_JQ,DXHD_QBYP,DXHD_RYBS,DXHD_RYGK_RYSMZ,DXHD_RYGK_ZDRYSMZDT,DXHD_RYTS_ZDRY,DXHD_SPSB,DXHD_TCC,DXHD_YJCZYA".split(","));
        strategy.setInclude("RBAC_GROUP,RBAC_USER,RBAC_USER_GROUP,SYSTEM_LOG,TRAIN_ABRY,TRAIN_ASJ,TRAIN_DTXL,TRAIN_DTZD,TRAIN_GJGWCYRY,TRAIN_GJXL,TRAIN_GJZD,TRAIN_JQDX,TRAIN_KLTJ,TRAIN_QWLL,TRAIN_RLYJ,TRAIN_WJP,TRAIN_XGAJTP,TRAIN_ZDRY".split(","));



        // 配置数据表与实体类名之间映射的策略
        strategy.setNaming(NamingStrategy.underline_to_camel);
        // 配置数据表的字段与实体类的属性名之间映射的策略
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        // 配置 lombok 模式
        strategy.setEntityLombokModel(true);
        // 配置 rest 风格的控制器（@RestController）
        strategy.setRestControllerStyle(true);
        // 配置驼峰转连字符
        strategy.setControllerMappingHyphenStyle(true);
        // 配置表前缀，生成实体时去除表前缀
        // 此处的表名为 test_mybatis_plus_user，模块名为 test_mybatis_plus，去除前缀后剩下为 user。
        strategy.setTablePrefix(pc.getModuleName() + "_");
        mpg.setStrategy(strategy);

        // Step6：执行代码生成操作
        mpg.execute();
    }

}