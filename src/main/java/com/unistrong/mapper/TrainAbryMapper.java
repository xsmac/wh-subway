package com.unistrong.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.unistrong.model.entity.TrainAbry;

/**
 * <p>
 * 地铁公交安保人员 Mapper 接口
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
public interface TrainAbryMapper extends BaseMapper<TrainAbry> {

}
