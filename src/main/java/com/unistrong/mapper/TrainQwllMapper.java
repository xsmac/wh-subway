package com.unistrong.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.unistrong.model.entity.TrainQwll;

/**
 * <p>
 * 地铁公交勤务力量 Mapper 接口
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
public interface TrainQwllMapper extends BaseMapper<TrainQwll> {

}
