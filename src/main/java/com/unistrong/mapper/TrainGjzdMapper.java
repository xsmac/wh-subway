package com.unistrong.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.unistrong.model.entity.TrainGjzd;

/**
 * <p>
 * 公交站点 Mapper 接口
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
public interface TrainGjzdMapper extends BaseMapper<TrainGjzd> {

}
