package com.unistrong.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.unistrong.model.entity.RbacUser;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
public interface RbacUserMapper extends BaseMapper<RbacUser> {

}
