package com.unistrong.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.unistrong.model.entity.TrainGjgwcyry;

/**
 * <p>
 * 关键岗位从业人员 Mapper 接口
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
public interface TrainGjgwcyryMapper extends BaseMapper<TrainGjgwcyry> {

}
