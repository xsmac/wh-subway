package com.unistrong.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.unistrong.model.entity.TrainWjp;

/**
 * <p>
 * 地铁公交违禁品 Mapper 接口
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
public interface TrainWjpMapper extends BaseMapper<TrainWjp> {

}
