package com.unistrong.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.unistrong.model.entity.TrainDtxl;

/**
 * <p>
 * 地铁线路 Mapper 接口
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
public interface TrainDtxlMapper extends BaseMapper<TrainDtxl> {

}
