package com.unistrong.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.unistrong.model.entity.TrainZdry;

/**
 * <p>
 * 地铁公交重点人员 Mapper 接口
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
public interface TrainZdryMapper extends BaseMapper<TrainZdry> {

}
