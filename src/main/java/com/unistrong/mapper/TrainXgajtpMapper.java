package com.unistrong.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.unistrong.model.entity.TrainXgajtp;

/**
 * <p>
 * X 光安检图片 Mapper 接口
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
public interface TrainXgajtpMapper extends BaseMapper<TrainXgajtp> {

}
