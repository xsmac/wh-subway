package com.unistrong.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.unistrong.model.entity.TrainJqdx;

/**
 * <p>
 * 地铁公交警情对象 Mapper 接口
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
public interface TrainJqdxMapper extends BaseMapper<TrainJqdx> {

}
