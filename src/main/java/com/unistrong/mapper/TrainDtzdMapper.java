package com.unistrong.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.unistrong.model.entity.TrainDtzd;

/**
 * <p>
 * 地铁站点 Mapper 接口
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
public interface TrainDtzdMapper extends BaseMapper<TrainDtzd> {

}
