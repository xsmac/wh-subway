package com.unistrong.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.unistrong.model.entity.TrainAsj;

/**
 * <p>
 * 地铁公交案事件 Mapper 接口
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
public interface TrainAsjMapper extends BaseMapper<TrainAsj> {

}
