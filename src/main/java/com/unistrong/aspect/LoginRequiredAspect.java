package com.unistrong.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoginRequiredAspect {

    @Pointcut("@annotation(com.unistrong.annotation.LoginRequired)")
    public void annotationPointcut() {

    }

    @AfterReturning("annotationPointcut()")
    public void doAfterReturning(JoinPoint joinPoint) {
        System.out.println("@LoginRequired生效中》》》》》》》》》》");
    }
}
