package com.unistrong.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.unistrong.model.entity.TrainRlyj;
import com.unistrong.model.entity.TrainZdry;
import com.unistrong.service.intf.TrainRlyjService;
import com.unistrong.util.Params;
import com.unistrong.util.R;
import com.unistrong.util.StringUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.util.Base64;
import java.util.Collection;
import java.util.UUID;

/**
 * <p>
 * 地铁公交人脸预警对象 前端控制器
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@RestController
@RequestMapping("/train-rlyj")
public class TrainRlyjController {

    @Autowired
    private TrainRlyjService rlyjService;

    @ApiOperation(value = "分页查询")
    @RequestMapping("selectPage")
    public R selectPage(@ApiIgnore @RequestBody Params<TrainRlyj> params){
        Page<TrainRlyj> page = new Page<>(params.getCurrent(),params.getSize());
        QueryWrapper<TrainRlyj> wrapper =new QueryWrapper<>();
        if(!StringUtils.isEmpty(params.getEntity().getZdryXm())){
            wrapper.like("ZDRY_XM",params.getEntity().getZdryXm());
        }
        IPage<TrainRlyj> pageVo = rlyjService.page(page,wrapper);
        for (TrainRlyj e: pageVo.getRecords()) {
            if(e.getZdryZprlzp()!=null){
                String base64Str = "data:image/jpg;base64,"+DatatypeConverter.printBase64Binary(e.getZdryZprlzp());
                e.setZdryZprlzpDz(base64Str);
            }
            if(e.getZdryBkrlzp()!=null){
                String base64Str = "data:image/jpg;base64,"+DatatypeConverter.printBase64Binary(e.getZdryBkrlzp());
                e.setZdryBkrlzpDz(base64Str);
            }
        }
        return R.ok().put("data",pageVo);

    }


    @ApiOperation(value = "根据id查询记录")
    @RequestMapping("getById")
    public R getById(@RequestParam String Id){
        TrainRlyj result = rlyjService.getById(Id);
        return R.ok().put("data",result);

    }

    @ApiOperation(value = "保存单条记录")
    @PostMapping("save")
    public R save(@RequestBody TrainRlyj entit){
        String id =entit.getId();
        if(id==null || id==""){
            entit.setId(UUID.randomUUID().toString());
        }
        Boolean result = rlyjService.save(entit);
        return R.ok().put("data",result);
    }


    @ApiOperation(value = "批量保存")
    @RequestMapping("saveBatch")
    public R saveBatch(@RequestBody Collection<TrainRlyj> entityList){
        Boolean result = rlyjService.saveBatch(entityList);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据 ID 选择修改")
    @RequestMapping("updateById")
    public R updateById(@RequestBody TrainRlyj entit){
        entit.setZdryZprlzp(null);
        entit.setZdryZprlzpDz(null);
        entit.setZdryBkrlzp(null);
        entit.setZdryBkrlzpDz(null);
        Boolean result = rlyjService.updateById(entit);
        return R.ok().put("data",result);

    }

    @ApiOperation(value = "根据 ID 选择修改")
    @RequestMapping("update")
    public R update(@RequestParam String id,@RequestParam("file") MultipartFile file) throws IOException {
        TrainRlyj entity = new TrainRlyj();
        entity.setZdryZprlzp(file.getBytes());
        if(!StringUtils.isEmpty(id) && !id.equals("undefined")){
            entity.setId(id);
            rlyjService.updateById(entity);
        }else{
            entity.setId(UUID.randomUUID().toString());
            rlyjService.save(entity);
        }
        return R.ok().put("data",entity.getId());
    }

    @ApiOperation(value = "根据 ID 选择修改")
    @RequestMapping("updateT")
    public R updateT(@RequestParam String id,@RequestParam("file") MultipartFile file) throws IOException {
        TrainRlyj entity = new TrainRlyj();
        entity.setZdryBkrlzp(file.getBytes());
        if(!StringUtils.isEmpty(id) && !id.equals("undefined")){
            entity.setId(id);
            rlyjService.updateById(entity);
        }else{
            entity.setId(UUID.randomUUID().toString());
            rlyjService.save(entity);
        }
        return R.ok().put("data",entity.getId());
    }


    @ApiOperation(value = "根据 ID 选择修改")
    @RequestMapping("handleRemove")
    public R handleRemove(@RequestBody TrainRlyj entity) throws IOException {
        //  DxhdRybs entity = new DxhdRybs();
        entity.setZdryZprlzp(new byte[0]);
        //  entity.setAjbh(ajbh);
        rlyjService.updateById(entity);
        return R.ok().put("data",entity.getId());
    }

    @ApiOperation(value = "根据 ID 选择修改")
    @RequestMapping("handleRemoveT")
    public R handleRemoveT(@RequestBody TrainRlyj entity) throws IOException {
        //  DxhdRybs entity = new DxhdRybs();
        entity.setZdryBkrlzp(new byte[0]);
        //  entity.setAjbh(ajbh);
        rlyjService.updateById(entity);
        return R.ok().put("data",entity.getId());
    }
    @ApiOperation(value = "根据ID 批量更新")
    @RequestMapping("updateBatchById")
    public R updateBatchById(@RequestBody Collection<TrainRlyj> entityList){
        Boolean result = rlyjService.updateBatchById(entityList);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据 ID 删除")
    @RequestMapping("removeById")
    public R removeById(@RequestBody TrainRlyj entity){
        Boolean result = rlyjService.removeById(entity.getId());
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "删除（根据ID 批量删除）")
    @RequestMapping("removeByIds")
    public R removeByIds(@RequestBody Collection<String> Ids){
        Boolean result = rlyjService.removeByIds(Ids);
        return R.ok().put("data",result);
    }
}

