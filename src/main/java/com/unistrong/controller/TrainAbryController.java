package com.unistrong.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.unistrong.model.entity.TrainAbry;
import com.unistrong.service.intf.TrainAbryService;
import com.unistrong.util.Params;
import com.unistrong.util.R;
import com.unistrong.util.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Collection;
import java.util.Date;
import java.util.UUID;

/**
 * <p>
 * 地铁公交安保人员 前端控制器
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Api(tags = {"TrainAbryController"},value = "安保人员信息",description = "通用接口")
@RestController
@RequestMapping("/train-abry")
public class TrainAbryController {

    @Autowired
    private TrainAbryService abryService;

    @ApiOperation(value = "分页查询")
    @RequestMapping("selectPage")
    public R selectPage(@ApiIgnore @RequestBody Params<TrainAbry> params){
        Page<TrainAbry> page = new Page<>(params.getCurrent(),params.getSize());
        QueryWrapper<TrainAbry> wrapper =new QueryWrapper<>();
        if(!StringUtils.isEmpty(params.getEntity().getAbryXm())){
            wrapper.like("ABRY_XM",params.getEntity().getAbryXm());
        }
        if(!StringUtils.isEmpty(params.getEntity().getAbryDwmc())){
            wrapper.like("ABRY_DWMC",params.getEntity().getAbryDwmc());
        }
        IPage<TrainAbry> pageVo = abryService.page(page,wrapper);
        return R.ok().put("data",pageVo);

    }


    @ApiOperation(value = "根据id查询记录")
    @RequestMapping("getById")
    public R getById(@RequestParam String Id){
        TrainAbry result = abryService.getById(Id);
        return R.ok().put("data",result);

    }

    @ApiOperation(value = "保存单条记录")
    @PostMapping("save")
    public R save(@RequestBody TrainAbry entit){
        entit.setCtm(new Date());
        entit.setId(UUID.randomUUID().toString());
        Boolean result = abryService.save(entit);
        return R.ok().put("data",result);
    }


    @ApiOperation(value = "批量保存")
    @RequestMapping("saveBatch")
    public R saveBatch(@RequestBody Collection<TrainAbry> entityList){
        Boolean result = abryService.saveBatch(entityList);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据 ID 选择修改")
    @RequestMapping("updateById")
    public R updateById(@RequestBody TrainAbry entit){
        Boolean result = abryService.updateById(entit);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据ID 批量更新")
    @RequestMapping("updateBatchById")
    public R updateBatchById(@RequestBody Collection<TrainAbry> entityList){
        Boolean result = abryService.updateBatchById(entityList);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据 ID 删除")
    @RequestMapping("removeById")
    public R removeById(@RequestBody TrainAbry entity){
        Boolean result = abryService.removeById(entity.getId());
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "删除（根据ID 批量删除）")
    @RequestMapping("removeByIds")
    public R removeByIds(@RequestBody Collection<String> Ids){
        Boolean result = abryService.removeByIds(Ids);
        return R.ok().put("data",result);
    }
}

