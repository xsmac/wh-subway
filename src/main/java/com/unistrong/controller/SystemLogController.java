package com.unistrong.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 系统日志 前端控制器
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@RestController
@RequestMapping("/system-log")
public class SystemLogController {

}

