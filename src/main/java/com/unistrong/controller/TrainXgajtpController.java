package com.unistrong.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.unistrong.model.entity.TrainXgajtp;
import com.unistrong.service.intf.TrainXgajtpService;
import com.unistrong.util.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;

/**
 * <p>
 * X 光安检图片 前端控制器
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@RestController
@RequestMapping("/train-xgajtp")
public class TrainXgajtpController {

    @Autowired
    private TrainXgajtpService xgajtpService;

    @ApiOperation(value = "分页查询")
    @RequestMapping("selectPage")
    public R selectPage(@ApiIgnore @RequestBody Params<TrainXgajtp> params){
        Page<TrainXgajtp> page = new Page<>(params.getCurrent(),params.getSize());
        QueryWrapper<TrainXgajtp> wrapper =new QueryWrapper<>();
        wrapper.select("id","XZQHDM","DTXLBM","(select dtxlmc from train_dtxl xl where xl.dtxlbh = x.DTXLBM) dtxlmc",
                "DTZDBM","(select dtzdmc from train_dtzd zd where zd.dtzdbm = x.DTZDBM) dtzdmc",
                "WJPLXDM","CCLJ","CJSJ","TPGS","TPDX","TP","TPDZ","CTU","CTM","UTU");
        //违禁品类型
        if(!StringUtils.isEmpty(params.getEntity().getWjplxdm())) {
            wrapper.eq("WJPLXDM",params.getEntity().getWjplxdm());

        }
        IPage<TrainXgajtp> pageVo = xgajtpService.page(page,wrapper);
        for (TrainXgajtp e: pageVo.getRecords()) {
            if(e.getTp()!=null){
                String base64Str = "data:image/jpg;base64,"+ DatatypeConverter.printBase64Binary(e.getTp());
                e.setTpdz(base64Str);
            }
        }
        return R.ok().put("data",pageVo);

    }


    @ApiOperation(value = "根据id查询记录")
    @RequestMapping("getById")
    public R getById(@RequestParam String Id){
        TrainXgajtp result = xgajtpService.getById(Id);
        return R.ok().put("data",result);

    }

    @ApiOperation(value = "保存单条记录")
    @PostMapping("save")
    public R save(@RequestBody TrainXgajtp entit){
        entit.setCjsj(DateUtils.getTime());
        if(StringUtils.isEmpty(entit.getId())){
            entit.setId(UUIDUtils.getUUID());
        }
        Boolean result = xgajtpService.save(entit);
        return R.ok().put("data",result);
    }


    @ApiOperation(value = "批量保存")
    @RequestMapping("saveBatch")
    public R saveBatch(@RequestBody Collection<TrainXgajtp> entityList){
        Boolean result = xgajtpService.saveBatch(entityList);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据 ID 选择修改")
    @RequestMapping("updateById")
    public R updateById(@RequestBody TrainXgajtp entit){
        entit.setUtm(new Date());
        entit.setTp(null);
        entit.setTpdz(null);
        entit.setDtxlmc(null);
        entit.setDtzdmc(null);
        Boolean result = xgajtpService.updateById(entit);
        return R.ok().put("data",result);

    }

    @ApiOperation(value = "根据 ID 选择修改")
    @RequestMapping("update")
    public R update(@RequestParam String id,@RequestParam("file") MultipartFile file) throws IOException {
        TrainXgajtp entity = new TrainXgajtp();
        entity.setTp(file.getBytes());
        if(!StringUtils.isEmpty(id) && !id.equals("undefined")){
            entity.setId(id);
            xgajtpService.updateById(entity);
        }else{
            entity.setId(UUIDUtils.getUUID());
            xgajtpService.save(entity);
        }
        return R.ok().put("data",entity.getId());
    }

    @ApiOperation(value = "根据 ID 选择修改")
    @RequestMapping("handleRemove")
    public R handleRemove(@RequestBody TrainXgajtp entity) throws IOException {
        entity.setTp(new byte[0]);
        xgajtpService.updateById(entity);
        return R.ok().put("data",entity.getId());
    }

    @ApiOperation(value = "根据ID 批量更新")
    @RequestMapping("updateBatchById")
    public R updateBatchById(@RequestBody Collection<TrainXgajtp> entityList){
        Boolean result = xgajtpService.updateBatchById(entityList);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据 ID 删除")
    @RequestMapping("removeById")
    public R removeById(@RequestParam String Id){
        Boolean result = xgajtpService.removeById(Id);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "删除（根据ID 批量删除）")
    @RequestMapping("removeByIds")
    public R removeByIds(@RequestBody Collection<String> Ids){
        Boolean result = xgajtpService.removeByIds(Ids);
        return R.ok().put("data",result);
    }
}

