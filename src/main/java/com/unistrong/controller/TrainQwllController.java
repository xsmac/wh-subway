package com.unistrong.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.unistrong.model.entity.TrainQwll;
import com.unistrong.service.intf.TrainQwllService;
import com.unistrong.util.Params;
import com.unistrong.util.R;
import com.unistrong.util.StringUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Collection;
import java.util.Date;
import java.util.UUID;

/**
 * <p>
 * 地铁公交勤务力量 前端控制器
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@RestController
@RequestMapping("/train-qwll")
public class TrainQwllController {

    @Autowired
    private TrainQwllService qwllService;

    @ApiOperation(value = "分页查询")
    @RequestMapping("selectPage")
    public R selectPage(@ApiIgnore @RequestBody Params<TrainQwll> params){
        Page<TrainQwll> page = new Page<>(params.getCurrent(),params.getSize());
        QueryWrapper<TrainQwll> wrapper =new QueryWrapper<>();
        if(!StringUtils.isEmpty(params.getEntity().getQwllXm())){
            wrapper.like("QWLL_XM",params.getEntity().getQwllXm());
        }
        if(!StringUtils.isEmpty(params.getEntity().getDtzdmc())){
            wrapper.like("DTZDMC",params.getEntity().getDtzdmc());
        }
        wrapper.orderByDesc("CTM");
        IPage<TrainQwll> pageVo = qwllService.page(page,wrapper);
        return R.ok().put("data",pageVo);

    }


    @ApiOperation(value = "根据id查询记录")
    @RequestMapping("getById")
    public R getById(@RequestParam String Id){
        TrainQwll result = qwllService.getById(Id);
        return R.ok().put("data",result);

    }

    @ApiOperation(value = "保存单条记录")
    @PostMapping("save")
    public R save(@RequestBody TrainQwll entit){
        entit.setId(UUID.randomUUID().toString());
        entit.setCtm(new Date());
        Boolean result = qwllService.save(entit);
        return R.ok().put("data",result);
    }


    @ApiOperation(value = "批量保存")
    @RequestMapping("saveBatch")
    public R saveBatch(@RequestBody Collection<TrainQwll> entityList){
        Boolean result = qwllService.saveBatch(entityList);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据 ID 选择修改")
    @RequestMapping("updateById")
    public R updateById(@RequestBody TrainQwll entit){
        Boolean result = qwllService.updateById(entit);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据ID 批量更新")
    @RequestMapping("updateBatchById")
    public R updateBatchById(@RequestBody Collection<TrainQwll> entityList){
        Boolean result = qwllService.updateBatchById(entityList);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据 ID 删除")
    @RequestMapping("removeById")
    public R removeById(@RequestBody TrainQwll entity){
        Boolean result = qwllService.removeById(entity.getId());
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "删除（根据ID 批量删除）")
    @RequestMapping("removeByIds")
    public R removeByIds(@RequestBody Collection<String> Ids){
        Boolean result = qwllService.removeByIds(Ids);
        return R.ok().put("data",result);
    }
}

