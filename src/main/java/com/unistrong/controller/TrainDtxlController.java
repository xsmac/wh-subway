package com.unistrong.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.unistrong.model.entity.TrainDtxl;
import com.unistrong.service.intf.TrainDtxlService;
import com.unistrong.util.Params;
import com.unistrong.util.R;
import com.unistrong.util.StringUtils;
import com.unistrong.util.UUIDUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Collection;
import java.util.Date;

/**
 * <p>
 * 地铁线路 前端控制器
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@RestController
@RequestMapping("/train-dtxl")
public class TrainDtxlController {

    @Autowired
    private TrainDtxlService dtxlService;

    @ApiOperation(value = "分页查询")
    @RequestMapping("selectPage")
    public R selectPage(@ApiIgnore @RequestBody Params<TrainDtxl> params){
        Page<TrainDtxl> page = new Page<>(params.getCurrent(),params.getSize());
        QueryWrapper<TrainDtxl> wrapper =new QueryWrapper<>();
        //地铁线路名称
        if(!StringUtils.isEmpty(params.getEntity().getDtxlmc())) {
            wrapper.like("DTXLMC",params.getEntity().getDtxlmc());

        }
        //起点站
        if(!StringUtils.isEmpty(params.getEntity().getDtxlqdz())) {
            wrapper.like("DTXLQDZ",params.getEntity().getDtxlqdz());

        }
        //终点站
        if(!StringUtils.isEmpty(params.getEntity().getDtxlzdz())) {
            wrapper.like("DTXLZDZ",params.getEntity().getDtxlzdz());

        }
        IPage<TrainDtxl> pageVo = dtxlService.page(page,wrapper);
        return R.ok().put("data",pageVo);

    }


    @ApiOperation(value = "根据id查询记录")
    @RequestMapping("getById")
    public R getById(@RequestParam String Id){
        TrainDtxl result = dtxlService.getById(Id);
        return R.ok().put("data",result);

    }

    @ApiOperation(value = "保存单条记录")
    @PostMapping("save")
    public R save(@RequestBody TrainDtxl entit){
        entit.setId(UUIDUtils.getUUID());
        Boolean result = dtxlService.save(entit);
        return R.ok().put("data",result);
    }


    @ApiOperation(value = "批量保存")
    @RequestMapping("saveBatch")
    public R saveBatch(@RequestBody Collection<TrainDtxl> entityList){
        Boolean result = dtxlService.saveBatch(entityList);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据 ID 选择修改")
    @RequestMapping("updateById")
    public R updateById(@RequestBody TrainDtxl entit){
        entit.setUtm(new Date());
        Boolean result = dtxlService.updateById(entit);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据ID 批量更新")
    @RequestMapping("updateBatchById")
    public R updateBatchById(@RequestBody Collection<TrainDtxl> entityList){
        Boolean result = dtxlService.updateBatchById(entityList);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据 ID 删除")
    @RequestMapping("removeById")
    public R removeById(@RequestParam String Id){
        Boolean result = dtxlService.removeById(Id);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "删除（根据ID 批量删除）")
    @RequestMapping("removeByIds")
    public R removeByIds(@RequestBody Collection<String> Ids){
        Boolean result = dtxlService.removeByIds(Ids);
        return R.ok().put("data",result);
    }
}

