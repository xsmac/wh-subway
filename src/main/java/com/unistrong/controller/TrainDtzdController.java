package com.unistrong.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.unistrong.model.entity.TrainDtzd;
import com.unistrong.service.intf.TrainDtzdService;
import com.unistrong.util.Params;
import com.unistrong.util.R;
import com.unistrong.util.StringUtils;
import com.unistrong.util.UUIDUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Collection;
import java.util.Date;

/**
 * <p>
 * 地铁站点 前端控制器
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@RestController
@RequestMapping("/train-dtzd")
public class TrainDtzdController {

    @Autowired
    private TrainDtzdService dtzdService;

    @ApiOperation(value = "分页查询")
    @RequestMapping("selectPage")
    public R selectPage(@ApiIgnore @RequestBody Params<TrainDtzd> params){
        Page<TrainDtzd> page = new Page<>(params.getCurrent(),params.getSize());
        QueryWrapper<TrainDtzd> wrapper =new QueryWrapper<>();
        wrapper.select("ID","XZQHDM","DTXLBM","(select dtxlmc from train_dtxl xl where xl.dtxlbh = DTXLBM) dtxlmc","DTZDBM",
                "DTZDMC","SDPCSDM","SDPCSMC","KSSJ","JSSJ","CTU","CTM","UTU","UTM");
        //地铁站点名称
        if(!StringUtils.isEmpty(params.getEntity().getDtzdmc())) {
            wrapper.like("DTZDMC",params.getEntity().getDtzdmc());

        }
        IPage<TrainDtzd> pageVo = dtzdService.page(page,wrapper);
        return R.ok().put("data",pageVo);

    }


    @ApiOperation(value = "根据id查询记录")
    @RequestMapping("getById")
    public R getById(@RequestParam String Id){
        TrainDtzd result = dtzdService.getById(Id);
        return R.ok().put("data",result);

    }

    @ApiOperation(value = "保存单条记录")
    @PostMapping("save")
    public R save(@RequestBody TrainDtzd entit){
        entit.setId(UUIDUtils.getUUID());
        Boolean result = dtzdService.save(entit);
        return R.ok().put("data",result);
    }


    @ApiOperation(value = "批量保存")
    @RequestMapping("saveBatch")
    public R saveBatch(@RequestBody Collection<TrainDtzd> entityList){
        Boolean result = dtzdService.saveBatch(entityList);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据 ID 选择修改")
    @RequestMapping("updateById")
    public R updateById(@RequestBody TrainDtzd entit){
        entit.setUtm(new Date());
        entit.setDtxlmc(null);
        Boolean result = dtzdService.updateById(entit);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据ID 批量更新")
    @RequestMapping("updateBatchById")
    public R updateBatchById(@RequestBody Collection<TrainDtzd> entityList){
        Boolean result = dtzdService.updateBatchById(entityList);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据 ID 删除")
    @RequestMapping("removeById")
    public R removeById(@RequestParam String Id){
        Boolean result = dtzdService.removeById(Id);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "删除（根据ID 批量删除）")
    @RequestMapping("removeByIds")
    public R removeByIds(@RequestBody Collection<String> Ids){
        Boolean result = dtzdService.removeByIds(Ids);
        return R.ok().put("data",result);
    }
}

