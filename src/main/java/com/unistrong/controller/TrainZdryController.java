package com.unistrong.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.unistrong.model.entity.TrainZdry;
import com.unistrong.service.intf.TrainZdryService;
import com.unistrong.util.Params;
import com.unistrong.util.R;
import com.unistrong.util.StringUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;

/**
 * <p>
 * 地铁公交重点人员 前端控制器
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@RestController
@RequestMapping("/train-zdry")
public class TrainZdryController {

    @Autowired
    private TrainZdryService zdryService;

    @ApiOperation(value = "分页查询")
    @RequestMapping("selectPage")
    public R selectPage(@ApiIgnore @RequestBody Params<TrainZdry> params){
        Page<TrainZdry> page = new Page<>(params.getCurrent(),params.getSize());
        QueryWrapper<TrainZdry> wrapper =new QueryWrapper<>();
        if(!StringUtils.isEmpty(params.getEntity().getZdryXm())){
            wrapper.like("ZDRY_XM",params.getEntity().getZdryXm());
        }
        IPage<TrainZdry> pageVo = zdryService.page(page,wrapper);
        for (TrainZdry e: pageVo.getRecords()) {
            if(e.getZdryZp()!=null){
                String base64Str = "data:image/jpg;base64,"+DatatypeConverter.printBase64Binary(e.getZdryZp());
                e.setZdryZpDz(base64Str);
            }
        }
        return R.ok().put("data",pageVo);

    }


    @ApiOperation(value = "根据id查询记录")
    @RequestMapping("getById")
    public R getById(@RequestParam String Id){
        TrainZdry result = zdryService.getById(Id);
        return R.ok().put("data",result);

    }

    @ApiOperation(value = "保存单条记录")
    @PostMapping("save")
    public R save(@RequestBody TrainZdry entit){
        String id =entit.getId();
        if(id==null || id==""){
            entit.setId(UUID.randomUUID().toString());
        }
        entit.setCtm(new Date());
        Boolean result = zdryService.save(entit);
        return R.ok().put("data",result);
    }


    @ApiOperation(value = "批量保存")
    @RequestMapping("saveBatch")
    public R saveBatch(@RequestBody Collection<TrainZdry> entityList){
        Boolean result = zdryService.saveBatch(entityList);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据 ID 选择修改")
    @RequestMapping("updateById")
    public R updateById(@RequestBody TrainZdry entit){
        entit.setZdryZp(null);
        entit.setZdryZpDz(null);
        Boolean result = zdryService.updateById(entit);
        return R.ok().put("data",result);

    }

    @ApiOperation(value = "根据 ID 选择修改")
    @RequestMapping("update")
    public R update(@RequestParam String id,@RequestParam("file") MultipartFile file) throws IOException {
        TrainZdry entity = new TrainZdry();
        entity.setZdryZp(file.getBytes());
        if(!StringUtils.isEmpty(id) && !id.equals("undefined")){
            entity.setId(id);
            zdryService.updateById(entity);
        }else{
            entity.setId(UUID.randomUUID().toString());
            zdryService.save(entity);
        }
        return R.ok().put("data",entity.getId());
    }

    @ApiOperation(value = "根据 ID 选择修改")
    @RequestMapping("handleRemove")
    public R handleRemove(@RequestBody TrainZdry entity) throws IOException {
        //  DxhdRybs entity = new DxhdRybs();
        entity.setZdryZp(new byte[0]);
        //  entity.setAjbh(ajbh);
        zdryService.updateById(entity);
        return R.ok().put("data",entity.getId());
    }
    @ApiOperation(value = "根据ID 批量更新")
    @RequestMapping("updateBatchById")
    public R updateBatchById(@RequestBody Collection<TrainZdry> entityList){
        Boolean result = zdryService.updateBatchById(entityList);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据 ID 删除")
    @RequestMapping("removeById")
    public R removeById(@RequestBody TrainZdry entity){
        Boolean result = zdryService.removeById(entity.getId());
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "删除（根据ID 批量删除）")
    @RequestMapping("removeByIds")
    public R removeByIds(@RequestBody Collection<String> Ids){
        Boolean result = zdryService.removeByIds(Ids);
        return R.ok().put("data",result);
    }
}

