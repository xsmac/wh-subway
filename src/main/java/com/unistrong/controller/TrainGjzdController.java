package com.unistrong.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.unistrong.model.entity.TrainGjzd;
import com.unistrong.service.intf.TrainGjzdService;
import com.unistrong.util.Params;
import com.unistrong.util.R;
import com.unistrong.util.StringUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Collection;
import java.util.Date;
import java.util.UUID;

/**
 * <p>
 * 公交站点 前端控制器
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@RestController
@RequestMapping("/train-gjzd")
public class TrainGjzdController {

    @Autowired
    private TrainGjzdService gjzdService;

    @ApiOperation(value = "分页查询")
    @RequestMapping("selectPage")
    public R selectPage(@ApiIgnore @RequestBody Params<TrainGjzd> params){
        Page<TrainGjzd> page = new Page<>(params.getCurrent(),params.getSize());
        QueryWrapper<TrainGjzd> wrapper =new QueryWrapper<>();
        if(!StringUtils.isEmpty(params.getEntity().getGjzdmc())){
            wrapper.like("GJZDMC",params.getEntity().getGjzdmc());
        }
        //由于没有字典，暂时使用上面（名称代替编码）
//        if(!StringUtils.isEmpty(params.getEntity().getGjzdbm())){
//            wrapper.eq("GJZDMC",params.getEntity().getGjzdbm());
//        }
        if(!StringUtils.isEmpty(params.getEntity().getKssj())){
            wrapper.ge("KSSJ",params.getEntity().getKssj().substring(2,21));
            wrapper.le("KSSJ",params.getEntity().getKssj().substring(24,43));
        }
        IPage<TrainGjzd> pageVo = gjzdService.page(page,wrapper);
        return R.ok().put("data",pageVo);

    }


    @ApiOperation(value = "根据id查询记录")
    @RequestMapping("getById")
    public R getById(@RequestParam String Id){
        TrainGjzd result = gjzdService.getById(Id);
        return R.ok().put("data",result);

    }

    @ApiOperation(value = "保存单条记录")
    @PostMapping("save")
    public R save(@RequestBody TrainGjzd entit){
        entit.setCtm(new Date());
        entit.setId(UUID.randomUUID().toString());
        Boolean result = gjzdService.save(entit);
        return R.ok().put("data",result);
    }


    @ApiOperation(value = "批量保存")
    @RequestMapping("saveBatch")
    public R saveBatch(@RequestBody Collection<TrainGjzd> entityList){
        Boolean result = gjzdService.saveBatch(entityList);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据 ID 选择修改")
    @RequestMapping("updateById")
    public R updateById(@RequestBody TrainGjzd entit){
        Boolean result = gjzdService.updateById(entit);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据ID 批量更新")
    @RequestMapping("updateBatchById")
    public R updateBatchById(@RequestBody Collection<TrainGjzd> entityList){
        Boolean result = gjzdService.updateBatchById(entityList);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据 ID 删除")
    @RequestMapping("removeById")
    public R removeById(@RequestBody TrainGjzd entity){
        Boolean result = gjzdService.removeById(entity.getId());
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "删除（根据ID 批量删除）")
    @RequestMapping("removeByIds")
    public R removeByIds(@RequestBody Collection<String> Ids){
        Boolean result = gjzdService.removeByIds(Ids);
        return R.ok().put("data",result);
    }
}

