package com.unistrong.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.unistrong.model.entity.TrainGjxl;
import com.unistrong.service.intf.TrainGjxlService;
import com.unistrong.util.Params;
import com.unistrong.util.R;
import com.unistrong.util.StringUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Collection;
import java.util.Date;
import java.util.UUID;

/**
 * <p>
 * 公交线路 前端控制器
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@RestController
@RequestMapping("/train-gjxl")
public class TrainGjxlController {

    @Autowired
    private TrainGjxlService gjxlService;

    @ApiOperation(value = "分页查询")
    @RequestMapping("selectPage")
    public R selectPage(@ApiIgnore @RequestBody Params<TrainGjxl> params){
        Page<TrainGjxl> page = new Page<>(params.getCurrent(),params.getSize());
        QueryWrapper<TrainGjxl> wrapper =new QueryWrapper<>();
        if(!StringUtils.isEmpty(params.getEntity().getGjxlmc())){
            wrapper.like("GJXLMC",params.getEntity().getGjxlmc());
        }
        wrapper.orderByDesc("CTM");
        //公交线路编码，暂时没有字典，不可用
//        if(!StringUtils.isEmpty(params.getEntity().getGjxlbm())){
//            wrapper.like("GJXLBM",params.getEntity().getGjxlbm());
//        }
        IPage<TrainGjxl> pageVo = gjxlService.page(page,wrapper);
        return R.ok().put("data",pageVo);

    }


    @ApiOperation(value = "根据id查询记录")
    @RequestMapping("getById")
    public R getById(@RequestParam String Id){
        TrainGjxl result = gjxlService.getById(Id);
        return R.ok().put("data",result);

    }

    @ApiOperation(value = "保存单条记录")
    @PostMapping("save")
    public R save(@RequestBody TrainGjxl entit){
        entit.setId(UUID.randomUUID().toString());
        entit.setCtm(new Date());
        Boolean result = gjxlService.save(entit);
        return R.ok().put("data",result);
    }


    @ApiOperation(value = "批量保存")
    @RequestMapping("saveBatch")
    public R saveBatch(@RequestBody Collection<TrainGjxl> entityList){
        Boolean result = gjxlService.saveBatch(entityList);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据 ID 选择修改")
    @RequestMapping("updateById")
    public R updateById(@RequestBody TrainGjxl entit){
        Boolean result = gjxlService.updateById(entit);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据ID 批量更新")
    @RequestMapping("updateBatchById")
    public R updateBatchById(@RequestBody Collection<TrainGjxl> entityList){
        Boolean result = gjxlService.updateBatchById(entityList);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据 ID 删除")
    @RequestMapping("removeById")
    public R removeById(@RequestBody TrainGjxl entity){
        Boolean result = gjxlService.removeById(entity.getId());
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "删除（根据ID 批量删除）")
    @RequestMapping("removeByIds")
    public R removeByIds(@RequestBody Collection<String> Ids){
        Boolean result = gjxlService.removeByIds(Ids);
        return R.ok().put("data",result);
    }
}

