package com.unistrong.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.unistrong.model.entity.TrainAsj;
import com.unistrong.service.intf.TrainAsjService;
import com.unistrong.util.DateFormat;
import com.unistrong.util.Params;
import com.unistrong.util.R;
import com.unistrong.util.StringUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Collection;
import java.util.Date;
import java.util.UUID;

/**
 * <p>
 * 地铁公交案事件 前端控制器
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@RestController
@RequestMapping("/train-asj")
public class TrainAsjController {

    @Autowired
    private TrainAsjService asjService;

    @ApiOperation(value = "分页查询")
    @RequestMapping("selectPage")
    public R selectPage(@ApiIgnore @RequestBody Params<TrainAsj> params){
        Page<TrainAsj> page = new Page<>(params.getCurrent(),params.getSize());
        QueryWrapper<TrainAsj> wrapper =new QueryWrapper<>();
        if(!StringUtils.isEmpty(params.getEntity().getAjmc())){
            wrapper.like("AJMC",params.getEntity().getAjmc());
        }
        if(!StringUtils.isEmpty(params.getEntity().getDtzdmc())){
            wrapper.like("DTZDMC",params.getEntity().getDtzdmc());
        }
        wrapper.orderByDesc("ASJFSKSSJ");
        IPage<TrainAsj> pageVo = asjService.page(page,wrapper);
        return R.ok().put("data",pageVo);

    }


    @ApiOperation(value = "根据id查询记录")
    @RequestMapping("getById")
    public R getById(@RequestParam String Id){
        TrainAsj result = asjService.getById(Id);
        return R.ok().put("data",result);

    }

    @ApiOperation(value = "保存单条记录")
    @PostMapping("save")
    public R save(@RequestBody TrainAsj entit){
        entit.setId(UUID.randomUUID().toString());
        entit.setCtm(new Date());
        Boolean result = asjService.save(entit);
        return R.ok().put("data",result);
    }


    @ApiOperation(value = "批量保存")
    @RequestMapping("saveBatch")
    public R saveBatch(@RequestBody Collection<TrainAsj> entityList){
        Boolean result = asjService.saveBatch(entityList);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据 ID 选择修改")
    @RequestMapping("updateById")
    public R updateById(@RequestBody TrainAsj entit){
        Boolean result = asjService.updateById(entit);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据ID 批量更新")
    @RequestMapping("updateBatchById")
    public R updateBatchById(@RequestBody Collection<TrainAsj> entityList){
        Boolean result = asjService.updateBatchById(entityList);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据 ID 删除")
    @RequestMapping("removeById")
    public R removeById(@RequestBody TrainAsj entity){
        Boolean result = asjService.removeById(entity.getId());
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "删除（根据ID 批量删除）")
    @RequestMapping("removeByIds")
    public R removeByIds(@RequestBody Collection<String> Ids){
        Boolean result = asjService.removeByIds(Ids);
        return R.ok().put("data",result);
    }
}

