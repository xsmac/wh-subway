package com.unistrong.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.unistrong.model.entity.TrainKltj;
import com.unistrong.service.intf.TrainKltjService;
import com.unistrong.util.Params;
import com.unistrong.util.R;
import com.unistrong.util.StringUtils;
import com.unistrong.util.UUIDUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Collection;
import java.util.Date;

/**
 * <p>
 * 客流统计 前端控制器
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@RestController
@RequestMapping("/train-kltj")
public class TrainKltjController {

    @Autowired
    private TrainKltjService kltjService;

    @ApiOperation(value = "分页查询")
    @RequestMapping("selectPage")
    public R selectPage(@ApiIgnore @RequestBody Params<TrainKltj> params){
        Page<TrainKltj> page = new Page<>(params.getCurrent(),params.getSize());
        QueryWrapper<TrainKltj> wrapper =new QueryWrapper<>();
        wrapper.select("id","XZQHDM","DTXLBM","(select dtxlmc from train_dtxl xl where xl.dtxlbh = DTXLBM) dtxlmc",
                "DTZDBM","(select dtzdmc from train_dtzd zd where zd.dtzdbm = kl.DTZDBM) dtzdmc",
                "DTJZKLL","DTCZKLL","KLLTJRQ","CTU","CTM","UTU","UTM");
        //地铁站点名称
        if(!StringUtils.isEmpty(params.getEntity().getKlltjrq())) {
            wrapper.like("KLLTJRQ",params.getEntity().getKlltjrq());

        }
        wrapper.orderByDesc("KLLTJRQ","DTZDBM");
        IPage<TrainKltj> pageVo = kltjService.page(page,wrapper);
        return R.ok().put("data",pageVo);

    }


    @ApiOperation(value = "根据id查询记录")
    @RequestMapping("getById")
    public R getById(@RequestParam String Id){
        TrainKltj result = kltjService.getById(Id);
        return R.ok().put("data",result);

    }

    @ApiOperation(value = "保存单条记录")
    @PostMapping("save")
    public R save(@RequestBody TrainKltj entit){
        entit.setId(UUIDUtils.getUUID());
        Boolean result = kltjService.save(entit);
        return R.ok().put("data",result);
    }


    @ApiOperation(value = "批量保存")
    @RequestMapping("saveBatch")
    public R saveBatch(@RequestBody Collection<TrainKltj> entityList){
        Boolean result = kltjService.saveBatch(entityList);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据 ID 选择修改")
    @RequestMapping("updateById")
    public R updateById(@RequestBody TrainKltj entit){
        entit.setUtm(new Date());
        entit.setDtxlmc(null);
        entit.setDtzdmc(null);
        Boolean result = kltjService.updateById(entit);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据ID 批量更新")
    @RequestMapping("updateBatchById")
    public R updateBatchById(@RequestBody Collection<TrainKltj> entityList){
        Boolean result = kltjService.updateBatchById(entityList);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据 ID 删除")
    @RequestMapping("removeById")
    public R removeById(@RequestParam String Id){
        Boolean result = kltjService.removeById(Id);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "删除（根据ID 批量删除）")
    @RequestMapping("removeByIds")
    public R removeByIds(@RequestBody Collection<String> Ids){
        Boolean result = kltjService.removeByIds(Ids);
        return R.ok().put("data",result);
    }
}

