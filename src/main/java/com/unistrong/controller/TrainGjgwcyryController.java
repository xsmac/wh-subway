package com.unistrong.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.unistrong.model.entity.TrainGjgwcyry;
import com.unistrong.service.intf.TrainGjgwcyryService;
import com.unistrong.util.Params;
import com.unistrong.util.R;
import com.unistrong.util.StringUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Collection;
import java.util.Date;
import java.util.UUID;

/**
 * <p>
 * 关键岗位从业人员 前端控制器
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@RestController
@RequestMapping("/train-gjgwcyry")
public class TrainGjgwcyryController {

    @Autowired
    private TrainGjgwcyryService gjgwcyryService;

    @ApiOperation(value = "分页查询")
    @RequestMapping("selectPage")
    public R selectPage(@ApiIgnore @RequestBody Params<TrainGjgwcyry> params){
        Page<TrainGjgwcyry> page = new Page<>(params.getCurrent(),params.getSize());
        QueryWrapper<TrainGjgwcyry> wrapper =new QueryWrapper<>();
        if(!StringUtils.isEmpty(params.getEntity().getGjgwcyryXm())){
            wrapper.like("GJGWCYRY_XM",params.getEntity().getGjgwcyryXm());
        }
        if(!StringUtils.isEmpty(params.getEntity().getGjgwcyryGwmc())){
            wrapper.like("GJGWCYRY_GWMC",params.getEntity().getGjgwcyryGwmc());
        }
        IPage<TrainGjgwcyry> pageVo = gjgwcyryService.page(page,wrapper);
        return R.ok().put("data",pageVo);

    }


    @ApiOperation(value = "根据id查询记录")
    @RequestMapping("getById")
    public R getById(@RequestParam String Id){
        TrainGjgwcyry result = gjgwcyryService.getById(Id);
        return R.ok().put("data",result);

    }

    @ApiOperation(value = "保存单条记录")
    @PostMapping("save")
    public R save(@RequestBody TrainGjgwcyry entit){
        entit.setId(UUID.randomUUID().toString());
        entit.setCtm(new Date());
        Boolean result = gjgwcyryService.save(entit);
        return R.ok().put("data",result);
    }


    @ApiOperation(value = "批量保存")
    @RequestMapping("saveBatch")
    public R saveBatch(@RequestBody Collection<TrainGjgwcyry> entityList){
        Boolean result = gjgwcyryService.saveBatch(entityList);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据 ID 选择修改")
    @RequestMapping("updateById")
    public R updateById(@RequestBody TrainGjgwcyry entit){
        Boolean result = gjgwcyryService.updateById(entit);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据ID 批量更新")
    @RequestMapping("updateBatchById")
    public R updateBatchById(@RequestBody Collection<TrainGjgwcyry> entityList){
        Boolean result = gjgwcyryService.updateBatchById(entityList);
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "根据 ID 删除")
    @RequestMapping("removeById")
    public R removeById(@RequestBody TrainGjgwcyry entity){
        Boolean result = gjgwcyryService.removeById(entity.getId());
        return R.ok().put("data",result);

    }


    @ApiOperation(value = "删除（根据ID 批量删除）")
    @RequestMapping("removeByIds")
    public R removeByIds(@RequestBody Collection<String> Ids){
        Boolean result = gjgwcyryService.removeByIds(Ids);
        return R.ok().put("data",result);
    }
}

