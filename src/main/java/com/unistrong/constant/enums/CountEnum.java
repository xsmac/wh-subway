package com.unistrong.constant.enums;


import lombok.Getter;

public enum CountEnum {

    ONE(1, "北京"), TWO(2, "上海"), THREE(3, "武汉"), FOUR(4, "重庆"), FIVE(5, "吉林"), SIX(6, "宁波");

    @Getter
    private Integer code;
    @Getter
    private String name;

    CountEnum(Integer code, String name){
        this.code=code;
        this.name=name;
    }

    public static CountEnum getCountEnum(Integer index){

        CountEnum[] array = CountEnum.values();
      /*  for (int i = 0; i < array.length; i++) {
            if(index==array[i].getCode()){
                return array[i];
            }
        }*/

        for (CountEnum enums:array) {
            if(index==enums.getCode()){
                return enums;
            }
        }

        return null;
    }
}
