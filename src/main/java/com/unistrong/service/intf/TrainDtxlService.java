package com.unistrong.service.intf;

import com.baomidou.mybatisplus.extension.service.IService;
import com.unistrong.model.entity.TrainDtxl;

/**
 * <p>
 * 地铁线路 服务类
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
public interface TrainDtxlService extends IService<TrainDtxl> {

}
