package com.unistrong.service.intf;

import com.baomidou.mybatisplus.extension.service.IService;
import com.unistrong.model.entity.TrainWjp;

/**
 * <p>
 * 地铁公交违禁品 服务类
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
public interface TrainWjpService extends IService<TrainWjp> {

}
