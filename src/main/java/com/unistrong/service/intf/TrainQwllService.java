package com.unistrong.service.intf;

import com.baomidou.mybatisplus.extension.service.IService;
import com.unistrong.model.entity.TrainQwll;

/**
 * <p>
 * 地铁公交勤务力量 服务类
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
public interface TrainQwllService extends IService<TrainQwll> {

}
