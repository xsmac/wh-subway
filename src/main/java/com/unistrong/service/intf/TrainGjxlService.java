package com.unistrong.service.intf;

import com.baomidou.mybatisplus.extension.service.IService;
import com.unistrong.model.entity.TrainGjxl;

/**
 * <p>
 * 公交线路 服务类
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
public interface TrainGjxlService extends IService<TrainGjxl> {

}
