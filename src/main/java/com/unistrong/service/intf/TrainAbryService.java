package com.unistrong.service.intf;

import com.baomidou.mybatisplus.extension.service.IService;
import com.unistrong.model.entity.TrainAbry;

/**
 * <p>
 * 地铁公交安保人员 服务类
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
public interface TrainAbryService extends IService<TrainAbry> {

}
