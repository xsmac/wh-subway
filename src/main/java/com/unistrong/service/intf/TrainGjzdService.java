package com.unistrong.service.intf;

import com.baomidou.mybatisplus.extension.service.IService;
import com.unistrong.model.entity.TrainGjzd;

/**
 * <p>
 * 公交站点 服务类
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
public interface TrainGjzdService extends IService<TrainGjzd> {

}
