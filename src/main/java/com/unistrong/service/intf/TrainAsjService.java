package com.unistrong.service.intf;

import com.baomidou.mybatisplus.extension.service.IService;
import com.unistrong.model.entity.TrainAsj;

/**
 * <p>
 * 地铁公交案事件 服务类
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
public interface TrainAsjService extends IService<TrainAsj> {

}
