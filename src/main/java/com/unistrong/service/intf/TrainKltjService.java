package com.unistrong.service.intf;

import com.baomidou.mybatisplus.extension.service.IService;
import com.unistrong.model.entity.TrainKltj;

/**
 * <p>
 * 客流统计 服务类
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
public interface TrainKltjService extends IService<TrainKltj> {

}
