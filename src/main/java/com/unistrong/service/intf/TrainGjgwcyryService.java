package com.unistrong.service.intf;

import com.baomidou.mybatisplus.extension.service.IService;
import com.unistrong.model.entity.TrainGjgwcyry;

/**
 * <p>
 * 关键岗位从业人员 服务类
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
public interface TrainGjgwcyryService extends IService<TrainGjgwcyry> {

}
