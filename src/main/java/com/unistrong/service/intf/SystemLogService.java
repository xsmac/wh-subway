package com.unistrong.service.intf;

import com.baomidou.mybatisplus.extension.service.IService;
import com.unistrong.model.entity.SystemLog;

/**
 * <p>
 * 系统日志 服务类
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
public interface SystemLogService extends IService<SystemLog> {

}
