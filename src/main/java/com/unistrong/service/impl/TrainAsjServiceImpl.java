package com.unistrong.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.unistrong.mapper.TrainAsjMapper;
import com.unistrong.model.entity.TrainAsj;
import com.unistrong.service.intf.TrainAsjService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 地铁公交案事件 服务实现类
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Service
public class TrainAsjServiceImpl extends ServiceImpl<TrainAsjMapper, TrainAsj> implements TrainAsjService {

}
