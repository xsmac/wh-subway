package com.unistrong.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.unistrong.mapper.TrainQwllMapper;
import com.unistrong.model.entity.TrainQwll;
import com.unistrong.service.intf.TrainQwllService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 地铁公交勤务力量 服务实现类
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Service
public class TrainQwllServiceImpl extends ServiceImpl<TrainQwllMapper, TrainQwll> implements TrainQwllService {

}
