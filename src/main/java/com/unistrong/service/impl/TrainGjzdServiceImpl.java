package com.unistrong.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.unistrong.mapper.TrainGjzdMapper;
import com.unistrong.model.entity.TrainGjzd;
import com.unistrong.service.intf.TrainGjzdService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 公交站点 服务实现类
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Service
public class TrainGjzdServiceImpl extends ServiceImpl<TrainGjzdMapper, TrainGjzd> implements TrainGjzdService {

}
