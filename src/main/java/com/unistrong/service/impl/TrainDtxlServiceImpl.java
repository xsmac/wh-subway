package com.unistrong.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.unistrong.mapper.TrainDtxlMapper;
import com.unistrong.model.entity.TrainDtxl;
import com.unistrong.service.intf.TrainDtxlService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 地铁线路 服务实现类
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Service
public class TrainDtxlServiceImpl extends ServiceImpl<TrainDtxlMapper, TrainDtxl> implements TrainDtxlService {

}
