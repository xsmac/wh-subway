package com.unistrong.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.unistrong.mapper.TrainGjgwcyryMapper;
import com.unistrong.model.entity.TrainGjgwcyry;
import com.unistrong.service.intf.TrainGjgwcyryService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 关键岗位从业人员 服务实现类
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Service
public class TrainGjgwcyryServiceImpl extends ServiceImpl<TrainGjgwcyryMapper, TrainGjgwcyry> implements TrainGjgwcyryService {

}
