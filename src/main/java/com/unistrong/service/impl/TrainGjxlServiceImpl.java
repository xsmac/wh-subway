package com.unistrong.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.unistrong.mapper.TrainGjxlMapper;
import com.unistrong.model.entity.TrainGjxl;
import com.unistrong.service.intf.TrainGjxlService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 公交线路 服务实现类
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Service
public class TrainGjxlServiceImpl extends ServiceImpl<TrainGjxlMapper, TrainGjxl> implements TrainGjxlService {

}
