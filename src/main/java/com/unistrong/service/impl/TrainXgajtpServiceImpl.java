package com.unistrong.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.unistrong.mapper.TrainXgajtpMapper;
import com.unistrong.model.entity.TrainXgajtp;
import com.unistrong.service.intf.TrainXgajtpService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * X 光安检图片 服务实现类
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Service
public class TrainXgajtpServiceImpl extends ServiceImpl<TrainXgajtpMapper, TrainXgajtp> implements TrainXgajtpService {

}
