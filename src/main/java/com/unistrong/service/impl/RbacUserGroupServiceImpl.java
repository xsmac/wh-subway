package com.unistrong.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.unistrong.mapper.RbacUserGroupMapper;
import com.unistrong.model.entity.RbacUserGroup;
import com.unistrong.service.intf.RbacUserGroupService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Service
public class RbacUserGroupServiceImpl extends ServiceImpl<RbacUserGroupMapper, RbacUserGroup> implements RbacUserGroupService {

}
