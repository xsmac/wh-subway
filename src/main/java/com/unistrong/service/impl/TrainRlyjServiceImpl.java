package com.unistrong.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.unistrong.mapper.TrainRlyjMapper;
import com.unistrong.model.entity.TrainRlyj;
import com.unistrong.service.intf.TrainRlyjService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 地铁公交人脸预警对象 服务实现类
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Service
public class TrainRlyjServiceImpl extends ServiceImpl<TrainRlyjMapper, TrainRlyj> implements TrainRlyjService {

}
