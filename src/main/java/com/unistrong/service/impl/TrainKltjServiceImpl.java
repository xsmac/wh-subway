package com.unistrong.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.unistrong.mapper.TrainKltjMapper;
import com.unistrong.model.entity.TrainKltj;
import com.unistrong.service.intf.TrainKltjService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 客流统计 服务实现类
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Service
public class TrainKltjServiceImpl extends ServiceImpl<TrainKltjMapper, TrainKltj> implements TrainKltjService {

}
