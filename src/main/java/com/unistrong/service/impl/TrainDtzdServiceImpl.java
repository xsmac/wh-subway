package com.unistrong.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.unistrong.mapper.TrainDtzdMapper;
import com.unistrong.model.entity.TrainDtzd;
import com.unistrong.service.intf.TrainDtzdService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 地铁站点 服务实现类
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Service
public class TrainDtzdServiceImpl extends ServiceImpl<TrainDtzdMapper, TrainDtzd> implements TrainDtzdService {

}
