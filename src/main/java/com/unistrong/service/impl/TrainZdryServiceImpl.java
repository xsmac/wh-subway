package com.unistrong.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.unistrong.mapper.TrainZdryMapper;
import com.unistrong.model.entity.TrainZdry;
import com.unistrong.service.intf.TrainZdryService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 地铁公交重点人员 服务实现类
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Service
public class TrainZdryServiceImpl extends ServiceImpl<TrainZdryMapper, TrainZdry> implements TrainZdryService {

}
