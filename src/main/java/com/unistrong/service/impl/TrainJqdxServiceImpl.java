package com.unistrong.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.unistrong.mapper.TrainJqdxMapper;
import com.unistrong.model.entity.TrainJqdx;
import com.unistrong.service.intf.TrainJqdxService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 地铁公交警情对象 服务实现类
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Service
public class TrainJqdxServiceImpl extends ServiceImpl<TrainJqdxMapper, TrainJqdx> implements TrainJqdxService {

}
