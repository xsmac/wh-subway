package com.unistrong.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.unistrong.mapper.RbacGroupMapper;
import com.unistrong.model.entity.RbacGroup;
import com.unistrong.service.intf.RbacGroupService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户-组 服务实现类
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Service
public class RbacGroupServiceImpl extends ServiceImpl<RbacGroupMapper, RbacGroup> implements RbacGroupService {

}
