package com.unistrong.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.unistrong.mapper.TrainAbryMapper;
import com.unistrong.model.entity.TrainAbry;
import com.unistrong.service.intf.TrainAbryService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 地铁公交安保人员 服务实现类
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Service
public class TrainAbryServiceImpl extends ServiceImpl<TrainAbryMapper, TrainAbry> implements TrainAbryService {

}
