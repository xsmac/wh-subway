package com.unistrong.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.unistrong.mapper.TrainWjpMapper;
import com.unistrong.model.entity.TrainWjp;
import com.unistrong.service.intf.TrainWjpService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 地铁公交违禁品 服务实现类
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Service
public class TrainWjpServiceImpl extends ServiceImpl<TrainWjpMapper, TrainWjp> implements TrainWjpService {

}
