package com.unistrong.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * X 光安检图片
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("TRAIN_XGAJTP x")
@ApiModel(value="TrainXgajtp对象", description="X 光安检图片")
public class TrainXgajtp implements Serializable {

    private static final long serialVersionUID=1L;

    @TableField("ID")
    private String id;

    @ApiModelProperty(value = "行政区划代码")
    @TableField("XZQHDM")
    private String xzqhdm;

    @ApiModelProperty(value = "地铁线路编码")
    @TableField("DTXLBM")
    private String dtxlbm;

    private String dtxlmc;

    @ApiModelProperty(value = "地铁站点编码")
    @TableField("DTZDBM")
    private String dtzdbm;

    private String dtzdmc;

    @ApiModelProperty(value = " 违禁品类型代码")
    @TableField("WJPLXDM")
    private String wjplxdm;

    @ApiModelProperty(value = "存储路径")
    @TableField("CCLJ")
    private String cclj;

    @ApiModelProperty(value = "采集时间")
    @TableField("CJSJ")
    private String cjsj;

    @ApiModelProperty(value = "图片格式")
    @TableField("TPGS")
    private String tpgs;

    @ApiModelProperty(value = "图片大小")
    @TableField("TPDX")
    private String tpdx;

    @ApiModelProperty(value = "图片")
    @TableField("TP")
    private byte[] tp;

    @ApiModelProperty(value = "图片地址")
    @TableField("TPDZ")
    private String tpdz;

    @TableField("CTU")
    private String ctu;

    @TableField("CTM")
    private Date ctm;

    @TableField("UTU")
    private String utu;

    @TableField("UTM")
    private Date utm;


}
