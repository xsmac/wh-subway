package com.unistrong.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 地铁公交勤务力量
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("TRAIN_QWLL")
@ApiModel(value="TrainQwll对象", description="地铁公交勤务力量")
public class TrainQwll implements Serializable {

    private static final long serialVersionUID=1L;

    @TableField("ID")
    private String id;

    @ApiModelProperty(value = "公安机关机构代码")
    @TableField("GAJGJGDM")
    private String gajgjgdm;

    @ApiModelProperty(value = "地铁站点编码")
    @TableField("DTZDBM")
    private String dtzdbm;

    @ApiModelProperty(value = "地铁站点名称")
    @TableField("DTZDMC")
    private String dtzdmc;

    @ApiModelProperty(value = "公交线路编码")
    @TableField("GJXLBM")
    private String gjxlbm;

    @ApiModelProperty(value = "公交公司名称")
    @TableField("GJGSMC")
    private String gjgsmc;

    @ApiModelProperty(value = "勤务力量_类型")
    @TableField("QWLL_LX")
    private String qwllLx;

    @ApiModelProperty(value = "勤务力量_姓名")
    @TableField("QWLL_XM")
    private String qwllXm;

    @ApiModelProperty(value = "勤务力量_联系电话")
    @TableField("QWLL_LXDH")
    private String qwllLxdh;

    @ApiModelProperty(value = "警员编号")
    @TableField("JYBH")
    private String jybh;

    @ApiModelProperty(value = "勤务类别代码")
    @TableField("QWLBDM")
    private String qwlbdm;

    @TableField("CTU")
    private String ctu;

    @TableField("CTM")
    private Date ctm;

    @TableField("UTU")
    private String utu;

    @TableField("UTM")
    private Date utm;


}
