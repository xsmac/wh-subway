package com.unistrong.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 地铁公交人脸预警对象
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("TRAIN_RLYJ")
@ApiModel(value="TrainRlyj对象", description="地铁公交人脸预警对象")
public class TrainRlyj implements Serializable {

    private static final long serialVersionUID=1L;

    @TableField("ID")
    private String id;

    @ApiModelProperty(value = "公安机关机构代码")
    @TableField("GAJGJGDM")
    private String gajgjgdm;

    @ApiModelProperty(value = "地铁线路代码")
    @TableField("DTXLDM")
    private String dtxldm;

    @ApiModelProperty(value = "地铁站点名称")
    @TableField("DTZDMC")
    private String dtzdmc;

    @ApiModelProperty(value = "公交线路代码")
    @TableField("GJXLDM")
    private String gjxldm;

    @ApiModelProperty(value = "分类代码")
    @TableField("ZDRY_FLDM")
    private String zdryFldm;

    @ApiModelProperty(value = "姓名")
    @TableField("ZDRY_XM")
    private String zdryXm;

    @ApiModelProperty(value = "公民身份号码")
    @TableField("ZDRY_GMSFHM")
    private String zdryGmsfhm;

    @ApiModelProperty(value = "抓拍人脸照片")
    @TableField("ZDRY_ZPRLZP")
    private byte[] zdryZprlzp;

    //private String zdryZprlzpBase64;

    @ApiModelProperty(value = "布控人脸照片")
    @TableField("ZDRY_BKRLZP")
    private byte[] zdryBkrlzp;

    //private String zdryBkrlzpBase64;

    @ApiModelProperty(value = "预警时间")
    @TableField("YJSJ")
    private String yjsj;

    @TableField("CTU")
    private String ctu;

    @TableField("CTM")
    private Date ctm;

    @TableField("UTU")
    private String utu;

    @TableField("UTM")
    private Date utm;

    @ApiModelProperty(value = "抓拍人脸照片存储地址")
    @TableField("ZDRY_ZPRLZP_DZ")
    private String zdryZprlzpDz;

    @ApiModelProperty(value = "布控人脸照片存储地址")
    @TableField("ZDRY_BKRLZP_DZ")
    private String zdryBkrlzpDz;
}
