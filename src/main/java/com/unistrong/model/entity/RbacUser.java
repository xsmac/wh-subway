package com.unistrong.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.sql.Blob;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("RBAC_USER")
@ApiModel(value="RbacUser对象", description="")
public class RbacUser implements Serializable {

    private static final long serialVersionUID=1L;

    @TableField("USER_ID")
    private String userId;

    @TableField("USER_JH")
    private String userJh;

    @ApiModelProperty(value = "用户密码")
    @TableField("USER_PASSWORD")
    private String userPassword;

    @ApiModelProperty(value = "用户姓名")
    @TableField("USER_XM")
    private String userXm;

    @ApiModelProperty(value = "用户身份证号码")
    @TableField("USER_SFZH")
    private String userSfzh;

    @ApiModelProperty(value = "单位id")
    @TableField("USER_DWDM")
    private String userDwdm;

    @ApiModelProperty(value = "单位名称")
    @TableField("USER_DWMC")
    private String userDwmc;

    @ApiModelProperty(value = "级别")
    @TableField("USER_LEVEL")
    private Integer userLevel;

    @ApiModelProperty(value = "有效性(0:有效 1:无效)")
    @TableField("VALID")
    private Integer valid;

    @ApiModelProperty(value = "创建人")
    @TableField("CTU")
    private String ctu;

    @ApiModelProperty(value = "创建时间")
    @TableField("CTM")
    private Date ctm;

    @ApiModelProperty(value = "修改人")
    @TableField("UTU")
    private String utu;

    @ApiModelProperty(value = "修改时间")
    @TableField("UTM")
    private Date utm;

    @ApiModelProperty(value = "照片")
    @TableField("USER_XP")
    private Blob userXp;

    @ApiModelProperty(value = "用户手机号")
    @TableField("USER_MOBILE")
    private String userMobile;


}
