package com.unistrong.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 地铁公交警情对象
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("TRAIN_JQDX")
@ApiModel(value="TrainJqdx对象", description="地铁公交警情对象")
public class TrainJqdx implements Serializable {

    private static final long serialVersionUID=1L;

    @TableField("ID")
    private String id;

    @ApiModelProperty(value = "公安机关机构代码")
    @TableField("GAJGJGDM")
    private String gajgjgdm;

    @ApiModelProperty(value = "地铁站点编码")
    @TableField("DTZDDM")
    private String dtzddm;

    @ApiModelProperty(value = "地铁站点名称")
    @TableField("DTZDMC")
    private String dtzdmc;

    @ApiModelProperty(value = "公交线路代码")
    @TableField("GJXLDM")
    private String gjxldm;

    @ApiModelProperty(value = "接警编号")
    @TableField("JJBH")
    private String jjbh;

    @ApiModelProperty(value = "报警方式代码")
    @TableField("BJFSDM")
    private String bjfsdm;

    @ApiModelProperty(value = "警情类别代码")
    @TableField("JQLBDM")
    private String jqlbdm;

    @ApiModelProperty(value = "姓名")
    @TableField("BJR_XM")
    private String bjrXm;

    @ApiModelProperty(value = "性别代码")
    @TableField("BJR_XBDM")
    private String bjrXbdm;

    @ApiModelProperty(value = "联系电话")
    @TableField("BJR_LXDH")
    private String bjrLxdh;

    @ApiModelProperty(value = "报警电话")
    @TableField("BJDH")
    private String bjdh;

    @ApiModelProperty(value = "报警时间")
    @TableField("BJSJ")
    private String bjsj;

    @ApiModelProperty(value = "简要警情")
    @TableField("JYJQ")
    private String jyjq;

    @TableField("CTU")
    private String ctu;

    @TableField("CTM")
    private Date ctm;

    @TableField("UTU")
    private String utu;

    @TableField("UTM")
    private Date utm;


}
