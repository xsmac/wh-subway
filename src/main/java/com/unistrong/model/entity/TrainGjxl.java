package com.unistrong.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 公交线路
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("TRAIN_GJXL")
@ApiModel(value="TrainGjxl对象", description="公交线路")
public class TrainGjxl implements Serializable {

    private static final long serialVersionUID=1L;

    @TableField("ID")
    private String id;

    @ApiModelProperty(value = "行政区划代码")
    @TableField("XZQHDM")
    private String xzqhdm;

    @ApiModelProperty(value = "公交线路编码")
    @TableField("GJXLBM")
    private String gjxlbm;

    @ApiModelProperty(value = "公交线路名称")
    @TableField("GJXLMC")
    private String gjxlmc;

    @ApiModelProperty(value = "起点站首末车_开始时间")
    @TableField("QDZSMC_KSSJ")
    private String qdzsmcKssj;

    @ApiModelProperty(value = "起点站首末车_结束时间")
    @TableField("QDZSMC_JSSJ")
    private String qdzsmcJssj;

    @ApiModelProperty(value = "终点站首末车_开始时间")
    @TableField("ZDZSMC_KSSJ")
    private String zdzsmcKssj;

    @ApiModelProperty(value = "终点站首末车_结束时间")
    @TableField("ZDZSMC_JSSJ")
    private String zdzsmcJssj;

    @ApiModelProperty(value = "公交线路起点站")
    @TableField("GJXLQDZ")
    private String gjxlqdz;

    @TableField("CTU")
    private String ctu;

    @TableField("CTM")
    private Date ctm;

    @TableField("UTU")
    private String utu;

    @TableField("UTM")
    private Date utm;


}
