package com.unistrong.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户-组
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("RBAC_GROUP")
@ApiModel(value="RbacGroup对象", description="用户-组")
public class RbacGroup implements Serializable {

    private static final long serialVersionUID=1L;

    @TableField("ID")
    private String id;

    @ApiModelProperty(value = "组名称")
    @TableField("GROUP_NAME")
    private String groupName;

    @ApiModelProperty(value = "描述")
    @TableField("GROUP_DESC")
    private String groupDesc;

    @ApiModelProperty(value = "有效性(0:有效 1:无效)")
    @TableField("VALID")
    private String valid;

    @TableField("CTU")
    private String ctu;

    @TableField("CTM")
    private Date ctm;

    @TableField("UTU")
    private String utu;

    @TableField("UTM")
    private Date utm;


}
