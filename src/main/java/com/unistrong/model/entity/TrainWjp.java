package com.unistrong.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 地铁公交违禁品
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("TRAIN_WJP")
@ApiModel(value="TrainWjp对象", description="地铁公交违禁品")
public class TrainWjp implements Serializable {

    private static final long serialVersionUID=1L;

    @TableField("ID")
    private String id;

    @ApiModelProperty(value = "公安机关机构代码")
    @TableField("GAJGJGDM")
    private String gajgjgdm;

    @ApiModelProperty(value = "地铁线路代码")
    @TableField("DTXLDM")
    private String dtxldm;

    @ApiModelProperty(value = "地铁站点名称")
    @TableField("DTZDMC")
    private String dtzdmc;

    @ApiModelProperty(value = "违禁品类型代码")
    @TableField("WJPLXDM")
    private String wjplxdm;

    @ApiModelProperty(value = "采集时间")
    @TableField("CJSJ")
    private String cjsj;

    @ApiModelProperty(value = "姓名")
    @TableField("CYR_XM")
    private String cyrXm;

    @ApiModelProperty(value = "公民身份号码")
    @TableField("CYR_GMSFHM")
    private String cyrGmsfhm;

    @ApiModelProperty(value = "联系电话")
    @TableField("CYR_LXDH")
    private String cyrLxdh;

    @TableField("CTU")
    private String ctu;

    @TableField("CTM")
    private Date ctm;

    @TableField("UTU")
    private String utu;

    @TableField("UTM")
    private Date utm;


}
