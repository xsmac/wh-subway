package com.unistrong.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;

/**
 * <p>
 * 地铁公交重点人员
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("TRAIN_ZDRY")
@ApiModel(value="TrainZdry对象", description="地铁公交重点人员")
public class TrainZdry implements Serializable {

    private static final long serialVersionUID=1L;

    @TableField("ID")
    private String id;

    @ApiModelProperty(value = "行政区划代码")
    @TableField("XZQHDM")
    private String xzqhdm;

    @ApiModelProperty(value = "重点人员_公民身份 号码")
    @TableField("ZDRY_GMSFHM")
    private String zdryGmsfhm;

    @ApiModelProperty(value = "重点人员_姓名")
    @TableField("ZDRY_XM")
    private String zdryXm;

    @ApiModelProperty(value = "重点人员_政治面貌 代码")
    @TableField("ZDRY_ZZMMDM")
    private String zdryZzmmdm;

    @ApiModelProperty(value = "重点人员_职业")
    @TableField("ZDRY_ZY")
    private String zdryZy;

    @ApiModelProperty(value = "重点人员_性别代码")
    @TableField("ZDRY_XBDM")
    private String zdryXbdm;

    @ApiModelProperty(value = "重点人员_出生日期")
    @TableField("ZDRY_CSRQ")
    private String zdryCsrq;

    @ApiModelProperty(value = "重点人员_学历代码")
    @TableField("ZDRY_XLDM")
    private String zdryXldm;

    @ApiModelProperty(value = "重点人员_民族代码")
    @TableField("ZDRY_MZDM")
    private String zdryMzdm;

    @ApiModelProperty(value = "重点人员_现住址_地 址名称")
    @TableField("ZDRY_XZZ_DZMC")
    private String zdryXzzDzmc;

    @ApiModelProperty(value = "重点人员_重点人分 类编码")
    @TableField("ZDRY_ZDRFLBM")
    private String zdryZdrflbm;

    @ApiModelProperty(value = "重点人员_照片")
    @TableField("ZDRY_ZP")
    private byte[] zdryZp;

   // private String zdryZpBase64;

    @ApiModelProperty(value = "重点人员_照片存储地址")
    @TableField("ZDRY_ZP_DZ")
    private String zdryZpDz;

    @ApiModelProperty(value = "案件类别代码")
    @TableField("AJLBDM")
    private String ajlbdm;

    @ApiModelProperty(value = "案件编号")
    @TableField("AJBH")
    private String ajbh;

    @ApiModelProperty(value = "简要案情")
    @TableField("JYAQ")
    private String jyaq;

    @ApiModelProperty(value = "列管信息_列管人联 系电话")
    @TableField("LGXX_LGRLXDH")
    private String lgxxLgrlxdh;

    @ApiModelProperty(value = "列管信息_列管单位")
    @TableField("LGXX_LGDW")
    private String lgxxLgdw;

    @ApiModelProperty(value = "列管信息_列管时间")
    @TableField("LGXX_LGSJ")
    private String lgxxLgsj;

    @ApiModelProperty(value = "同案人员_姓名")
    @TableField("TARY_XM")
    private String taryXm;

    @ApiModelProperty(value = "同案人员_公民身份 号码")
    @TableField("TARY_GMSFHM")
    private String taryGmsfhm;

    @TableField("CTU")
    private String ctu;

    @TableField("CTM")
    private Date ctm;

    @TableField("UTU")
    private String utu;

    @TableField("UTM")
    private Date utm;


}
