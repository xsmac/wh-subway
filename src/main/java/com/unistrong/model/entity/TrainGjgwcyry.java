package com.unistrong.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 关键岗位从业人员
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("TRAIN_GJGWCYRY")
@ApiModel(value="TrainGjgwcyry对象", description="关键岗位从业人员")
public class TrainGjgwcyry implements Serializable {

    private static final long serialVersionUID=1L;

    @TableField("ID")
    private String id;

    @ApiModelProperty(value = "行政区域代码")
    @TableField("XZQYDM")
    private String xzqydm;

    @ApiModelProperty(value = "单位名称")
    @TableField("GJGWCYRY_DWMC")
    private String gjgwcyryDwmc;

    @ApiModelProperty(value = "企业部门")
    @TableField("GJGWCYRY_QYBM")
    private String gjgwcyryQybm;

    @ApiModelProperty(value = "岗位名称")
    @TableField("GJGWCYRY_GWMC")
    private String gjgwcyryGwmc;

    @ApiModelProperty(value = "公民身份号码")
    @TableField("GJGWCYRY_GMSFHM")
    private String gjgwcyryGmsfhm;

    @ApiModelProperty(value = "姓名")
    @TableField("GJGWCYRY_XM")
    private String gjgwcyryXm;

    @ApiModelProperty(value = "民族代码")
    @TableField("GJGWCYRY_MZDM")
    private String gjgwcyryMzdm;

    @ApiModelProperty(value = "违法犯罪经历描述")
    @TableField("GJGWCYRY_WFFZJLMS")
    private String gjgwcyryWffzjlms;

    @ApiModelProperty(value = "联系电话")
    @TableField("GJGWCYRY_LXDH")
    private String gjgwcyryLxdh;

    @ApiModelProperty(value = "地址名称")
    @TableField("GJGWCYRY_DZMC")
    private String gjgwcyryDzmc;

    @TableField("CTU")
    private String ctu;

    @TableField("CTM")
    private Date ctm;

    @TableField("UTU")
    private String utu;

    @TableField("UTM")
    private Date utm;


}
