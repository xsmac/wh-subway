package com.unistrong.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 地铁站点
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("TRAIN_DTZD")
@ApiModel(value="TrainDtzd对象", description="地铁站点")
public class TrainDtzd implements Serializable {

    private static final long serialVersionUID=1L;

    @TableField("ID")
    private String id;

    @ApiModelProperty(value = "行政区划代码")
    @TableField("XZQHDM")
    private String xzqhdm;

    @ApiModelProperty(value = "地铁线路编码")
    @TableField("DTXLBM")
    private String dtxlbm;

    private String dtxlmc;

    @ApiModelProperty(value = "地铁站点编码")
    @TableField("DTZDBM")
    private String dtzdbm;

    @ApiModelProperty(value = "地铁站点名称")
    @TableField("DTZDMC")
    private String dtzdmc;

    @ApiModelProperty(value = "属地派出所代码")
    @TableField("SDPCSDM")
    private String sdpcsdm;

    @ApiModelProperty(value = "属地派出所名称")
    @TableField("SDPCSMC")
    private String sdpcsmc;

    @ApiModelProperty(value = "开始时间")
    @TableField("KSSJ")
    private String kssj;

    @ApiModelProperty(value = "结束时间")
    @TableField("JSSJ")
    private String jssj;

    @TableField("CTU")
    private String ctu;

    @TableField("CTM")
    private Date ctm;

    @TableField("UTU")
    private String utu;

    @TableField("UTM")
    private Date utm;


}
