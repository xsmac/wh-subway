package com.unistrong.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 客流统计
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("TRAIN_KLTJ kl")
@ApiModel(value="TrainKltj对象", description="客流统计")
public class TrainKltj implements Serializable {

    private static final long serialVersionUID=1L;

    @TableField("ID")
    private String id;

    @ApiModelProperty(value = "行政区划代码")
    @TableField("XZQHDM")
    private String xzqhdm;

    @ApiModelProperty(value = "地铁线路编码")
    @TableField("DTXLBM")
    private String dtxlbm;

    private String dtxlmc;

    @ApiModelProperty(value = "地铁站点编码")
    @TableField("DTZDBM")
    private String dtzdbm;

    private String dtzdmc;

    @ApiModelProperty(value = "地铁进站客流量")
    @TableField("DTJZKLL")
    private String dtjzkll;

    @ApiModelProperty(value = "地铁出站客流量")
    @TableField("DTCZKLL")
    private String dtczkll;

    @ApiModelProperty(value = "客流量统计日期")
    @TableField("KLLTJRQ")
    private String klltjrq;

    @TableField("CTU")
    private String ctu;

    @TableField("CTM")
    private Date ctm;

    @TableField("UTU")
    private String utu;

    @TableField("UTM")
    private Date utm;


}
