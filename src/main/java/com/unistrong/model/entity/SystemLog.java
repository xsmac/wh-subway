package com.unistrong.model.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统日志
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("SYSTEM_LOG")
@ApiModel(value="SystemLog对象", description="系统日志")
public class SystemLog implements Serializable {

    private static final long serialVersionUID=1L;

    @TableField("ID")
    private String id;

    @TableField("USER_JH")
    private String userJh;

    @TableField("USER_XM")
    private String userXm;

    @TableField("USER_DWDM")
    private String userDwdm;

    @TableField("USER_DWMC")
    private String userDwmc;

    @ApiModelProperty(value = "1：登录日志 2 ：操作日志")
    @TableField("LOG_TYPE")
    private BigDecimal logType;

    @ApiModelProperty(value = "操作内容")
    @TableField("LOG_CONTENT")
    private String logContent;

    @ApiModelProperty(value = "操作详细  如 ：传递的参数")
    @TableField("LOG_DETAIL")
    private String logDetail;

    @TableField("LOG_DATE")
    private Date logDate;

    @TableField("CTM")
    private Date ctm;

    @TableField("IP")
    private String ip;

    @ApiModelProperty(value = "操作结果")
    @TableField("LOG_RESULT")
    private String logResult;

    @ApiModelProperty(value = "操作模块")
    @TableField("LOG_MODULE")
    private String logModule;

    @ApiModelProperty(value = "失败原因")
    @TableField("LOG_CAUSE")
    private String logCause;


}
