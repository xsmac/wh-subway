package com.unistrong.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("RBAC_USER_GROUP")
@ApiModel(value="RbacUserGroup对象", description="")
public class RbacUserGroup implements Serializable {

    private static final long serialVersionUID=1L;

    @TableField("ID")
    private String id;

    @TableField("USER_ID")
    private String userId;

    @TableField("GROUP_ID")
    private String groupId;

    @TableField("CTU")
    private String ctu;

    @TableField("CTM")
    private Date ctm;

    @ApiModelProperty(value = "有效性(0:有效 1:无效)")
    @TableField("VALID")
    private String valid;

    @TableField("UTU")
    private String utu;

    @TableField("UTM")
    private Date utm;


}
