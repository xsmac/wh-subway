package com.unistrong.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 地铁公交安保人员
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("TRAIN_ABRY")
@ApiModel(value="TrainAbry对象", description="地铁公交安保人员")
public class TrainAbry implements Serializable {

    private static final long serialVersionUID=1L;

    @TableField("ID")
    private String id;

    @ApiModelProperty(value = "公安机关机构代码")
    @TableField("GAJGJGDM")
    private String gajgjgdm;

    @ApiModelProperty(value = "地铁站点编码")
    @TableField("DTZDBM")
    private String dtzdbm;

    @ApiModelProperty(value = "地铁站点名称")
    @TableField("DTZDMC")
    private String dtzdmc;

    @ApiModelProperty(value = "公交线路编码")
    @TableField("GJXLBM")
    private String gjxlbm;

    @ApiModelProperty(value = "安保人员_公民身份 号码")
    @TableField("ABRY_GMSFHM")
    private String abryGmsfhm;

    @ApiModelProperty(value = "安保人员_姓名")
    @TableField("ABRY_XM")
    private String abryXm;

    @ApiModelProperty(value = "安保人员_移动电话")
    @TableField("ABRY_YDDH")
    private String abryYddh;

    @ApiModelProperty(value = "安保人员_政治面貌 代码")
    @TableField("ABRY_ZZMMDM")
    private String abryZzmmdm;

    @ApiModelProperty(value = "安保人员_民族代码")
    @TableField("ABRY_MZDM")
    private String abryMzdm;

    @ApiModelProperty(value = "安保人员_地址名称")
    @TableField("ABRY_DZMC")
    private String abryDzmc;

    @ApiModelProperty(value = "安保人员_单位名称")
    @TableField("ABRY_DWMC")
    private String abryDwmc;

    @TableField("CTU")
    private String ctu;

    @TableField("CTM")
    private Date ctm;

    @TableField("UTU")
    private String utu;

    @TableField("UTM")
    private Date utm;


}
