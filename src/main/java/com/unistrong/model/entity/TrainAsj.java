package com.unistrong.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 地铁公交案事件
 * </p>
 *
 * @author menu
 * @since 2021-08-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("TRAIN_ASJ")
@ApiModel(value="TrainAsj对象", description="地铁公交案事件")
public class TrainAsj implements Serializable {

    private static final long serialVersionUID=1L;

    @TableField("ID")
    private String id;

    @ApiModelProperty(value = "公安机关机构代码")
    @TableField("GAJGJGDM")
    private String gajgjgdm;

    @ApiModelProperty(value = "地铁站点编码")
    @TableField("DTZDBM")
    private String dtzdbm;

    @ApiModelProperty(value = "地铁站点名称")
    @TableField("DTZDMC")
    private String dtzdmc;

    @ApiModelProperty(value = "公交线路编码")
    @TableField("GJXLBM")
    private String gjxlbm;

    @ApiModelProperty(value = "公交公司名称")
    @TableField("GJGSMC")
    private String gjgsmc;

    @ApiModelProperty(value = "案件编号")
    @TableField("AJBH")
    private String ajbh;

    @ApiModelProperty(value = "案件类别")
    @TableField("AJLB")
    private String ajlb;

    @ApiModelProperty(value = "案件名称")
    @TableField("AJMC")
    private String ajmc;

    @ApiModelProperty(value = "简要案情")
    @TableField("JYAQ")
    private String jyaq;

    @ApiModelProperty(value = "案事件发生开始时间")
    @TableField("ASJFSKSSJ")
    private Date asjfskssj;

    @ApiModelProperty(value = "案发地点")
    @TableField("AFDD")
    private String afdd;

    @ApiModelProperty(value = "事发场所")
    @TableField("SFCS")
    private String sfcs;

    @ApiModelProperty(value = "所属辖区")
    @TableField("SSXQ")
    private String ssxq;

    @TableField("CTU")
    private String ctu;

    @TableField("CTM")
    private Date ctm;

    @TableField("UTU")
    private String utu;

    @TableField("UTM")
    private Date utm;


}
