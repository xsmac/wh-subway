package com.unistrong;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.unistrong.mapper")
public class WhMetroApplication {

    public static void main(String[] args) {
        SpringApplication.run(WhMetroApplication.class, args);
    }

}
