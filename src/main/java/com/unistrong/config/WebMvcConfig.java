//package com.unistrong.config;
//
//import com.unistrong.filter.AuthInterceptor;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.servlet.config.annotation.*;
//
//@Configuration
//public class WebMvcConfig implements WebMvcConfigurer {
//
//    @Autowired
//    private AuthInterceptor authInterceptor;
//    /**
//     * 开启跨域
//     */
//    @Override
//    public void addCorsMappings(CorsRegistry registry) {
//        // 设置允许跨域的路由
//        registry.addMapping("/**")
//                // 设置允许跨域请求的域名
//                .allowedOriginPatterns("*")
//                // 是否允许证书（cookies）
//                .allowCredentials(true)
//                // 设置允许的方法
//                .allowedMethods("*")
//                // 跨域允许时间
//                .maxAge(3600);
//    }
//
//
//
//
////    @Override
////    public void addInterceptors(InterceptorRegistry registry) {
////      // registry.addInterceptor(new AuthInterceptor()).addPathPatterns("/**");
////        InterceptorRegistration registration  = registry.addInterceptor(new AuthInterceptor());
////        registration.addPathPatterns("/**");
////        //添加不拦截路径
////        registration.excludePathPatterns(
////                "/doc.html",
////                "/v2/api-docs",
////                "/**/*.html",
////                "/**/*.js",
////                "/**/*.css",
////                "/**/*.woff",
////                "/**/*.ttf"
////        );
////    }
//
//
//}
