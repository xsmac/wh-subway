package com.unistrong.util;

import lombok.Data;

/**
 * @author
 * */
@Data
public class Params<Entity> {
    private Long current;
    private Long size;
    private Entity entity;
}
