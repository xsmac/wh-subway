package com.unistrong.util;

import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author
 * */
public class DateFormat {
        public static String format(Date data){
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return  simpleDateFormat.format(data);
        }
}
