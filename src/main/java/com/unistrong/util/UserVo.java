package com.unistrong.util;

import lombok.Data;

import java.util.List;

@Data
public class UserVo<T> {
    private Long current;
    private Long size;
    private Long total;
    private List<T> userList;
}